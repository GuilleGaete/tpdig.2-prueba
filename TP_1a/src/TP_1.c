/*! \file TP_1a.c
\brief Trabajo Practico numero 1

 *Name        : Tp1_b.c
 *
 *Author      : Guillermo Gaete, Marcelo Gaggino, Lucas Gomez, Mariano Heller
 *
 *Version     : 1.0
 *
 *Description : Toggle LED del stick con delay por hardware (SysTick + IRQ)

*/

/**
 * \mainpage
 *Name        : Tp1_b.c
 *
 *Author      : Guillermo Gaete, Marcelo Gaggino, Lucas Gomez, Mariano Heller
 *
 *Version     : 1.0
 *
 *Description : Toggle LED del stick con delay por hardware (SysTick + IRQ)
 */




#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>


/** Puerto del led*/
#define PUERTO_0		0
/** Pin del led*/
#define LED			    22

/**Macro de OUT para pinconfig*/
#define SALIDA 			1
/**Macro de IN para pinconfig*/
#define ENTRADA 		0

void pause_ms (int);
void config_hardware(void);

int g_contador=0;


/**Handler del Systick*/
void SysTick_Handler(void)
{
	if (g_contador) g_contador--;
}

/**Funcion auxiliar para el manejo de la pausa*/
void pause_ms(int t)
{
	g_contador=t;
	while(g_contador)__WFI();
}


/**
 *\brief Main. Inicializa Hw y entra en un while(1).
 *\brief Dentro del loop tooglea el led & inica la pausa.
 */

int main(void)
{

#if defined (__USE_LPCOPEN)
#if !defined(NO_BOARD_LIB)
    SystemCoreClockUpdate();
    Board_Init();
    Board_LED_Set(0, true);
#endif
#endif


    config_hardware();

        while(1)
        {
        	Chip_GPIO_SetPinToggle(LPC_GPIO,PUERTO_0,LED);
        	pause_ms(500);
        }
    return 0 ;
}

/**
 * \brief Funcion que configura el GPIO del led y el SysTick
 * \brief P(0,22)
 */

void config_hardware(void)
{
	SysTick_Config(SystemCoreClock / 1000);
	Chip_GPIO_Init(LPC_GPIO);
	Chip_GPIO_SetPinDIR(LPC_GPIO,PUERTO_0,LED,SALIDA);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,PUERTO_0,LED);
}
DSDSDSD