/*! \file Tp1_a.c
\brief Trabajo Practico numero 1

 *Name        : Tp1_a.c
 *
 *Author      : Guillermo Gaete, Marcelo Gaggino, Lucas Gomez, Mariano Heller
 *
 *Version     : 1.0
 *
 *Description : Toggle LED del stick con delay por software

*/


#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#define PUERTO_0		0
#define LED			    22

#define SALIDA 			1
#define ENTRADA 		0

void config_GPIO   (void);


int main(void)
{

#if defined (__USE_LPCOPEN)
#if !defined(NO_BOARD_LIB)
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
    // Set up and initialize all required blocks and
    // functions related to the board hardware
    Board_Init();
    // Set the LED to the state of "On"
    Board_LED_Set(0, true);
#endif
#endif


    config_GPIO();
    int a;

    while(1){
    	for(a=0; a<=10000000; a++);
    	Chip_GPIO_SetPinToggle(LPC_GPIO,PUERTO_0,LED);

    }
    return 0 ;
}

void config_GPIO (void)
{
	Chip_GPIO_Init(LPC_GPIO);
	Chip_GPIO_SetPinDIR(LPC_GPIO,PUERTO_0,LED,SALIDA);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,PUERTO_0,LED);
}
